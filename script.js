// Description modal
const itemDescritionLinks = document.querySelectorAll('.item-description-link');
const descriptionContent = document.querySelector('.description-content');
const descriptionImage = document.querySelector('.description-image');
const overlay = document.querySelector('.modal-backdrop');
const modal = document.querySelector('.modal');
const modalClose = document.querySelector('.close');
const body = document.querySelector('body');

itemDescritionLinks.forEach(item => {
	let description = item.closest('.flex-item').querySelector('.item-description').textContent;
	let descriptionImageSrc = item.closest('.flex-item').querySelector('.item-path') ? item.closest('.flex-item').querySelector('.item-path').textContent : '';
	item.addEventListener('click', (event) => {
		modal.classList.add('show');
		overlay.classList.add('show');
		body.classList.add('modal-open');
		descriptionImage.src = descriptionImageSrc;
		descriptionContent.innerText = description;
	})
});

modalClose.addEventListener('click', () => {
	modal.classList.remove('show');
	overlay.classList.remove('show');
	body.classList.remove('modal-open');
});

// Form service

const form = document.querySelector("#contact-form");
const inputs = form.querySelectorAll("[required]");
const userMessageContainer = document.querySelector(".user-message-container");
const submitBtn = document.querySelector("[type='submit']");

const checkEmailValidity = (value) => {
	const mailReg = /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*(\.\w{2,})+$/;
	return mailReg.test(value);
}

const checkValidity = (element) => {
	if (element.name === "email") {
		return checkEmailValidity(element.value);
	}
	
	return element.value !== "";
}

form.addEventListener("submit", e => {
    e.preventDefault();
	
let error;
for (const el of inputs) {
	if (!checkValidity(el)) {
		error = el.dataset.errorText;
		userMessageContainer.innerText = error;
		setTimeout(() => userMessageContainer.innerText = "", 5000);
		return;
	}
}

const formData = new FormData();
	for (const el of inputs) {
		formData.append(el.name, el.value);
	}

	const url = form.getAttribute("action"); //pobieramy adres wysyłki z action formularza
	const method = form.getAttribute("method"); //tak samo metodę
	
	submitBtn.disabled = true;
	submitBtn.classList.add("elem-is-busy");
 
	fetch(url, {
		method: method,
		body: formData
	})
	.then(res => res.json())
	.then(res => {
		//tutaj odpowiedź
	}).finally(() => { //gdy zakończy się połączenie chcemy włączyć przycisk submit
	   submitBtn.disabled = false;
	   submitBtn.classList.remove("elem-is-busy");
	   userMessageContainer.innerText = "Twój email został wysłany";
	}).catch(() => {
		userMessageContainer.innerText = "Wystąpił błąd spróbuj ponownie";
		setTimeout(() => userMessageContainer.innerText = "", 5000);
	})
});
